# Inherit from phone link config
TARGET_PHONE_LINK_SUPPORTED ?= true
ifeq ($(TARGET_PHONE_LINK_SUPPORTED),true)
# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/microsoft/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/microsoft/overlay/common

PRODUCT_PACKAGES += \
    CrossDeviceServiceBroker \
    DeviceIntegrationService \
    LinkToWindows \
    virtual_keyboard

# Device Integration
PRODUCT_SYSTEM_PROPERTIES += \
    persist.sys.device_integration.disable?=false
endif
